# radicale-docker

A simple no-config docker image for [Radicale](https://radicale.org/)

1. Mount a volume on `/data`.
2. Create authentication file at `/data/users`:

  `htpasswd -B -c /data/users user1`

3. Make a `/data/storage` folder to hold the data.
