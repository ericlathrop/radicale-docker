FROM python:3-alpine

RUN mkdir /app /data
WORKDIR /app

COPY requirements.txt /app

RUN apk add --no-cache libffi-dev python3-dev build-base git && \
  pip install --no-cache-dir -r requirements.txt && \
  apk del libffi-dev python3-dev build-base

EXPOSE 5232
CMD python3 -m radicale \
  --config "" \
  --server-hosts 0.0.0.0:5232 \
  --auth-type htpasswd \
  --auth-htpasswd-filename /data/users \
  --auth-htpasswd-encryption bcrypt \
  --storage-filesystem-folder /data/storage \
  --storage-hook "git add -A && (git diff --cached --quiet || git commit -m \"Changes by \"%(user)s)"
